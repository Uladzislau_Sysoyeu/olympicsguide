angular.module('starter.services', [])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Archery',
    firstAppearance: '1900th year',
    face: 'img/archery.png'
  }, {
    id: 1,
    name: 'Athletics',
    firstAppearance: '1896th year',
    face: 'img/athletics.png'
  }, {
    id: 2,
    name: 'Badminton',
    firstAppearance: '1992th year',
    face: 'img/badminton.png'
  }, {
    id: 3,
    name: 'Basketball',
    firstAppearance: '1936th year',
    face: 'img/basketball.png'
  }, {
    id: 4,
    name: 'Beach Volleyball',
    firstAppearance: '1996th year',
    face: 'img/beach_volley.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});
