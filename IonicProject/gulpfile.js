'use strict'

const gulp = require('gulp');
const gutil = require('gulp-util');
const bower = require('bower');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const minifyCss = require('gulp-minify-css');
const rename = require('gulp-rename');
const sh = require('shelljs');
const del = require('del');
const browsersync = require('browser-sync').create();
const merge = require('merge-stream');
const watch = require('gulp-watch');
const reload = browsersync.reload;
const exec = require('child_process').exec;
const eslint = require('gulp-eslint');
const stylish = require('eslint-stylish');


const paths = {
  angular: [
    './bower_components/angular/angular.js',
    './bower_components/angular-ui-router/release/angular-ui-router.js'
  ],
  vendors: [
    './bower_components/ionic/js/ionic.bundle.js',
  ],
  vendors_style: [
    './bower_components/ionic/css/ionic.css'
  ]
};

const config = {
  server: {
    baseDir: "./www"
  },
  tunnel: true,
  host: 'localhost',
  port: 8100,
  logPrefix: 'IonicProject'
};

gulp.task('lint', function() {
  return gulp.src('./src/app/**/*.js')
    .pipe(eslint('.eslintrc'))
    .pipe(eslint.format());
});

gulp.task('clean', function(){
  return del('./www/*', {
    force: true
  });
});

gulp.task('copy', function() {
  var index = gulp.src('./src/app/index.html')
    .pipe(gulp.dest('./www'));
  
  var images = gulp.src('./src/assets/img/**/*.*')
    .pipe(gulp.dest('./www/img'));
  
  return merge(index, images);
});

gulp.task('js-app', function() {
  return gulp.src(['./src/**/module.js', './src/**/*.js'])
    .pipe(concat('main.js'))
    .pipe(gulp.dest('./www'))
    .pipe(reload({stream: true}));
});

gulp.task('js', () => {
  
  var angular = gulp.src(paths.angular)
    .pipe(concat('angular.js'))
    .pipe(gulp.dest('./www'));
  
  var ionic = gulp.src(paths.vendors)
    .pipe(concat('vendors.js'))
    .pipe(gulp.dest('./www'));
  
  return merge(angular, ionic);
});

gulp.task('html', function() {
  return gulp.src('./src/app/**/*.html')
    .pipe(gulp.dest('./www/html'))
    .pipe(reload({stream: true}));
});

gulp.task('scss', function() {
  var scss = gulp.src('./src/assets/scss/main.scss')
    .pipe(sass())
    .pipe(concat('style.css'))
    .pipe(gulp.dest('./www'))
    .pipe(reload({stream: true}));
  
  var vendors = gulp.src(paths.vendors_style)
    .pipe(concat('vendors.css'))
    .pipe(gulp.dest('./www'));
  
  return merge (scss, vendors);
});

gulp.task('fonts', function() {
  return gulp.src(['./bower_components/ionic/fonts/*.*'])
    .pipe(gulp.dest('./www/fonts'));
})

gulp.task('webserver', function() {
  browsersync(config);
});

gulp.task('browserSync', function() {
  browsersync({
    server: {
      baseDir: "./www"
    },
    port: 8100,
    open: true,
    notify: false
  });
});

gulp.task('ionic', function() {
  exec('ionic serve', function(err, stdout, stderr) {});
});

gulp.task('watch', function(callback) {
  gulp.watch('./src/**/*.js', gulp.series('lint', 'js-app'));
  gulp.watch('./src/**/*.html', gulp.series('html'));
  gulp.watch('./src/**/*.scss', gulp.series('scss'));
  callback();
});

gulp.task('build', gulp.series('clean', gulp.parallel('lint', 'copy', 'js', 'js-app', 'html', 'scss', 'fonts')));

gulp.task('default', gulp.series('build', gulp.parallel('watch', 'ionic')));
