/* global window, cordova, StatusBar, angular */

(function() {
  'use strict';
  
  angular
    .module('starter', [
      'ionic',
      'config',
      'index',
      'service',
      'settings',
      'sports',
      'tabs',
      'history',
      'interceptors'
  ])
    .config(config)
    .run(run);

  /* @ngInject */
  function run($ionicPlatform) {
    $ionicPlatform.ready(function() {
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  }

  /* @ngInject */
  function config($urlRouterProvider, $httpProvider) {
    $httpProvider.interceptors.push('TokenInterceptor');
    
    $urlRouterProvider.otherwise('/tab/dash');
  }

})();
