/* global angular */

(function() {
  'use strict'
  angular
    .module('config', [])
    .constant('APP_CONFIG', {
      REST_ROOT: 'http://localhost:3000/',
      SPORT_MODAL_TEMPLATE: 'html/modules/settings/modals/add-sport/template.html',
      LOGIN_MODAL_TEMPLATE: 'html/modules/settings/modals/login/template.html'
    });
  
})();
