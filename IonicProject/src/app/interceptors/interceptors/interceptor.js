 /* global angular */

(function() {
  'use strict';

  angular
    .module('interceptors')
    .factory('TokenInterceptor', TokenInterceptor);
    
    function TokenInterceptor($window, $log, LStorage) {
      var service = {
        'request': onRequest,
        'response': onResponse
      };
      
      return service;
      
      function onRequest(config) {
        if (config.url.includes('.html')) {
          return config;
        }
        var token = LStorage.getObject('myKey').token;
        if (!token) {
          return config;
        }
        config.headers['Authorisation'] = token;
        
        return config;
      }

      function onResponse(response) {
        if ($window.ANGULAR_DEBUG) {
          $log.log(response);
        }
        return response;
      }

    }

})();
