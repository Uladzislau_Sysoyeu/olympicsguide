/* global angular */

(function() {
  'use strict';

  angular
    .module('history')
    .controller('HistoryCtrl', HistoryCtrl);

  /* @ngInject */
  function HistoryCtrl($scope, SportAPI, Pagination) {
    
    var olympPage = new Pagination({
      service: SportAPI.olympic,
      onDone: function() {
        if (olympPage.status().page === 1) {
          $scope.$broadcast('scroll.refreshComplete');
        } else {
          $scope.$broadcast('scroll.infiniteScrollComplete');
        }
      },
      key: 'olympics'
    });
    
    angular.extend($scope, {
      paging: olympPage,
      olympics: olympPage.items(),
      status: olympPage.status()
    });
    
    olympPage.next();
  }  
})();
