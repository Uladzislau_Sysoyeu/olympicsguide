/* global angular */

(function() {
  'use strict';

  angular
    .module('history', [])
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider

    .state('tab.history', {
      url: '/history',
      views: {
        'tab-history': {
          templateUrl: 'html/modules/history/history/template.html',
          controller: 'HistoryCtrl'
        }
      }
    })
    .state('tab.review', {
      url: '/history/:id',
      views: {
        'tab-history': {
          templateUrl: 'html/modules/history/review/template.html',
          controller: 'ReviewCtrl'
        }
      }
    });  
  }

})();
