/* global angular */

(function() {
  'use strict';

  angular
    .module('history')
    .controller('ReviewCtrl', ReviewCtrl);

  /* @ngInject */
  function ReviewCtrl($scope, $stateParams, SportAPI) {
    angular.extend($scope, {
      
    });
  
    SportAPI
      .review($stateParams.id)
      .then(function(response) {
        $scope.review = response.data.review;
      });
  }

})();
