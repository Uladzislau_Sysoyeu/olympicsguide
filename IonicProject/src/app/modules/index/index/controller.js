/* global angular */

(function() {
  'use strict';

  angular
    .module('index')
    .controller('DashCtrl', DashCtrl);

  /* @ngInject */
  function DashCtrl($scope, $stateParams, SportAPI) {
    angular.extend($scope, {
      
    });
  
    SportAPI
      .show()
      .then(function(response) {
        $scope.pictures = response.data.pictures;
      });
    
  }

})();
