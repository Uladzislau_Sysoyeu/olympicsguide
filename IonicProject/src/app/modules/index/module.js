/* global angular */

(function() {
  'use strict';

  angular
    .module('index', [])
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    //console.log('config');
    $stateProvider
    
    .state('tab.dash', {
      url: '/dash',
      views: {
        'tab-dash': {
          templateUrl: 'html/modules/index/index/template.html',
          controller: 'DashCtrl'
        }
      }
    });

  }

})();
