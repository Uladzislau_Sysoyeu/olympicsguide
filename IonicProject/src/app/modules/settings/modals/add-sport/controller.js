/* global angular */

(function() {
  'use strict';

  angular
    .module('settings')
    .controller('AddSportModalCtrl', AddSportModalCtrl); 
  
  function AddSportModalCtrl($scope, $state, SportAPI, AddSportModal) {

    angular.extend($scope, {
      sport: {}
    });

    angular.extend($scope, {
      create: create,
      close: close
    });
    
    function create(form) {
      if (form.$invalid) {
        return;
      }
      SportAPI
        .create($scope.sport)
        .then(function (/*response*/) {
          $state.go('tab.sports');  
        });
    }
    
    function close() {
      AddSportModal.close();
    }
  }
})();
