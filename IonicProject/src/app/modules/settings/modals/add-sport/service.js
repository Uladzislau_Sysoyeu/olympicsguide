/* global angular */

(function() {
  'use strict'

  angular
    .module('service')
    .factory('AddSportModal', AddSportModal);

  function AddSportModal($ionicModal, APP_CONFIG, $rootScope){
    var scope = $rootScope.$new();
    
    var service = {
      open: open,
      close: close
    };
    
    $ionicModal.fromTemplateUrl(APP_CONFIG.SPORT_MODAL_TEMPLATE, {
      animation: 'slide-in-up',
      scope: scope
    }).then(function (modal) {
      scope.AddSportModal = modal;
    });
    
    return service;

    function open() {
      return scope.AddSportModal.show();
    }

    function close() {
      return scope.AddSportModal.hide();
    } 
  }

})();
