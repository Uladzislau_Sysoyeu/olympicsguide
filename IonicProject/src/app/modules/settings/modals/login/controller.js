/* global angular */

(function() {
  'use strict';

  angular
    .module('settings')
    .controller('LoginModalCtrl', LoginModalCtrl);
  
  function LoginModalCtrl($scope, $ionicModal, $state, SportAPI, LStorage, LoginModal) {
    angular.extend($scope, {
      formAuth: {}
    });

    angular.extend($scope, {
      auth: auth,
      close: close
    });

    function auth(form){
      if (form.$invalid) {
        return;
      }
      SportAPI
        .auth($scope.formAuth)
        .then(function (response) {
          LStorage.setObject('myKey', response.data.tokenObject);
          $state.go('tab.sports');
      });
    }
    
    function close() {
      LoginModal.close();
    }
  }
})();
