/* global angular */

(function() {
  'use strict'

  angular
    .module('service')
    .factory('LoginModal', LoginModal);

  function LoginModal($ionicModal, APP_CONFIG, $rootScope){
    var scope = $rootScope.$new();
    scope.data = {};
    
    var service = {
      open: open,
      close: close
    };

    $ionicModal.fromTemplateUrl(APP_CONFIG.LOGIN_MODAL_TEMPLATE, {
      animation: 'slide-in-up',
      scope: scope
    }).then(function (modal) {
      scope.LoginModal = modal;
    }); 
    
    
    return service;

    function open() {
      scope.LoginModal.show();
    }

    function close() {  
      scope.LoginModal.hide();
    } 
  }

})();
