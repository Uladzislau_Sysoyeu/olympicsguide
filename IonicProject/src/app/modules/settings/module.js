/* global angular */

(function() {
  'use strict';

  angular
    .module('settings', [])
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider

    .state('tab.settings', {
      url: '/settings',
      views: {
        'tab-settings': {
          templateUrl: 'html/modules/settings/settings/template.html',
          controller: 'SettingsCtrl'
        }
      }
    });  
  }

})();
