/* global angular */

(function() {
  'use strict';

  angular
    .module('settings')
    .controller('SettingsCtrl', SettingsCtrl);

  /* @ngInject */
  function SettingsCtrl($scope, AddSportModal, LoginModal) {
    
   angular.extend($scope, {
      sportShow: sportShow,
      loginShow: loginShow
    });
    
    function sportShow() {
      AddSportModal.open();
    }
    
    function loginShow() {
      LoginModal.open();
    }
  }

})();
