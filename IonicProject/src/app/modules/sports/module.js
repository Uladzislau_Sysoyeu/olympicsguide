/* global angular */

(function() {
  'use strict';

  angular
    .module('sports', [])
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    //console.log('config');
    $stateProvider

    .state('tab.sports', {
      url: '/sports',
      views: {
        'tab-sports': {
          templateUrl: 'html/modules/sports/sports/template.html',
          controller: 'SportsCtrl'
        }
      }
    })
    .state('tab.sport-detail', {
      url: '/sports/:id',
      views: {
        'tab-sports': {
          templateUrl: 'html/modules/sports/sport/template.html',
          controller: 'SportDetailCtrl'
        }
      }
    });  
  }

})();
