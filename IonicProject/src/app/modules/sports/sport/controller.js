/* global angular */

(function() {
  'use strict';

  angular
    .module('sports')
    .controller('SportDetailCtrl', SportDetailCtrl);

  /* @ngInject */
  function SportDetailCtrl($scope, $stateParams, SportAPI) {
    angular.extend($scope, {
      
    });
  
    SportAPI
      .find($stateParams.id)
      .then(function(response) {
        $scope.sport = response.data.sport;
      });
  }

})();
