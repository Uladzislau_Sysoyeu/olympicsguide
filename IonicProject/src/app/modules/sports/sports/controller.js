/* global angular */

(function() {
  'use strict';

  angular
    .module('sports')
    .controller('SportsCtrl', SportsCtrl);

  /* @ngInject */
  function SportsCtrl($scope, $stateParams, SportAPI, Pagination, $state) {
    
    var paging = new Pagination({
      service: SportAPI.all,
      onDone: function() {
        if (paging.status().page === 1) {
          $scope.$broadcast('scroll.refreshComplete');
        } else {
          $scope.$broadcast('scroll.infiniteScrollComplete');
        }
      },
      key: 'sports'
    });
    
    angular.extend($scope, {
      paging: paging,
      sports: paging.items(),
      status: paging.status(),
      remove: remove
    });
    
    paging.next();
    
    function remove(index) {
      SportAPI
        .remove(paging.items()[index]['_id'])
        .then(function(/*response*/) {
          paging.items().splice(index, 1);
          $state.go('tab.sports');
        });
    }
  }  
})();
