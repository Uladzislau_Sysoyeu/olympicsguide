/* global angular */

(function() {
  'use strict';

  angular
    .module('tabs', [])
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    //console.log('config');
    $stateProvider

    .state('tab', {
      url: '/tab',
      abstract: true,
      templateUrl: 'html/modules/tabs/template.html'
    });
  }
  
})();
