/* global localStorage, angular */

(function() {
  'use strict'
  
  angular
    .module('service')
    .factory('LStorage', LStorage);
  
  function LStorage() {
    var LSservice = {
      setObject: setObject,
      getObject: getObject,
      setItem: setItem,
      getItem: getItem,
      clear: clear
    };
    
    return LSservice;
    
    function setObject(key, object) {
      var serializedObj = JSON.stringify(object);
      localStorage.setItem(key, serializedObj);
    }
    
    function getObject(key) {
      var stringObject = localStorage.getItem(key);
      if (stringObject) {
        try {
          var obj = JSON.parse(stringObject);
        }
        catch(err) {
          
        }
      }
      return obj;
    }
    
    function setItem(key, value) {
      localStorage.setItem(key, value);
    }
    
    function getItem(key) {
      return localStorage.getItem(key);
    }
    
    function clear() {
      localStorage.clear();
    }
  }

})();
