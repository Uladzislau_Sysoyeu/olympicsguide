/* global angular */
(function() {
  'use strict';

  angular
    .module('service')
    .factory('Pagination', Pagination);

  function Pagination($rootScope, $log) {

    var DEFAULTS = {
      limit: 10,
      offset: 0
    };

    function Paging(options) {
      this.options = options;

      this.service = options.service;
      this.key = options.key;
      this.onDone = options.onDone;
      this.params = angular.extend({}, DEFAULTS, options.params || {});
      this._items = [];
      this._status = {
        page: 0,
        progress: false,
        done: false,
        paginated: false
      };
    }

    angular.extend(Paging.prototype, {
      next: next,
      items: items,
      status: status,
      reload: reload
    });

    function next () {
      if (this._status.done || this._status.progress) {
        return;
      }
      this._status.progress = true;
      this._status.paginated = true;

      return this
        .service(this.params)
        .then(onSuccess().bind(this), onError.bind(this))
        .finally(onFinally.bind(this));
    }

    function onSuccess () {
      return function (response) {
        this._status.page++;
        Array.prototype.push.apply(this._items, response.data[this.key]);
        this.params.offset += response.data[this.key].length;
        if (response.data[this.key].length < this.params.limit) {
            this._status.done = true;
          }
        if (typeof this.onDone === 'function') {
          this.onDone();
        }
      }
    }

    function reload() {
      this._status.done = false;
      this._status.page = 0;
      this._status.paginated = false;
      this._items.length = 0;
      this.params.offset = 0;
      this.next();
    }

    function onError (error) {
      $log.log(error);
    }

    function onFinally () {
      this._status.progress = false;
    }

    function items () {
      return this._items;
    }

    function status () {
      return this._status;
    }

    return Paging;
  }

})();
