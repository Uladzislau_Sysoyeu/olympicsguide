/* global angular */

(function() {
  'use strict'
  
  angular
    .module('service')
    .factory('SportAPI', SportAPI);
  
  function SportAPI($http, APP_CONFIG){
    var service = {
      all: all,
      olympic: olympic,
      find: find,
      review: review,
      update: update,
      create: create,
      remove: remove,
      show: show,
      auth: auth
    };
    
    return service;
    
    function all(params) {
      var DEFAULTS = { limit: 10, offset: 0 };
      params = angular.extend(DEFAULTS, params);
      return $http.get(APP_CONFIG.REST_ROOT + 'sports?limit=' + params.limit + '&offset=' + params.offset);     
    }
    
    function olympic(params) {
      var DEFAULTS = { limit: 10, offset: 0 };
      params = angular.extend(DEFAULTS, params);
      return $http.get(APP_CONFIG.REST_ROOT + 'history?limit=' + params.limit + '&offset=' + params.offset);     
    }

    function find(id) {
      return $http.get(APP_CONFIG.REST_ROOT + 'sports/' + id); 
    } 
    
    function review(id) {
      return $http.get(APP_CONFIG.REST_ROOT + 'history/' + id); 
    } 
    
    function update() {
      return $http.get(APP_CONFIG.REST_ROOT + 'sports'); 
    }
    
    function create(sport) {
      return $http.post(APP_CONFIG.REST_ROOT + 'sports', { sport: sport }); 
    }
    
    function remove(id) {
      return $http.delete(APP_CONFIG.REST_ROOT + 'sports/' + id);
    }
    
    function show() {
      return $http.get(APP_CONFIG.REST_ROOT + 'pictures');
    }
    
    function auth(formAuth) {
      return $http.post(APP_CONFIG.REST_ROOT + 'auth', {formAuth: formAuth});
    }
  }

})();
