/* global angular */

(function() {
  'use strict';

  angular.module('interceptors', []);

})();
/* global angular */

(function() {
  'use strict';

  angular.module('service', []);

})();
/* global angular */

(function() {
  'use strict';

  angular
    .module('history', [])
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider

    .state('tab.history', {
      url: '/history',
      views: {
        'tab-history': {
          templateUrl: 'html/modules/history/history/template.html',
          controller: 'HistoryCtrl'
        }
      }
    })
    .state('tab.review', {
      url: '/history/:id',
      views: {
        'tab-history': {
          templateUrl: 'html/modules/history/review/template.html',
          controller: 'ReviewCtrl'
        }
      }
    });  
  }

})();

/* global angular */

(function() {
  'use strict';

  angular
    .module('index', [])
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    //console.log('config');
    $stateProvider
    
    .state('tab.dash', {
      url: '/dash',
      views: {
        'tab-dash': {
          templateUrl: 'html/modules/index/index/template.html',
          controller: 'DashCtrl'
        }
      }
    });

  }

})();

/* global angular */

(function() {
  'use strict';

  angular
    .module('settings', [])
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    $stateProvider

    .state('tab.settings', {
      url: '/settings',
      views: {
        'tab-settings': {
          templateUrl: 'html/modules/settings/settings/template.html',
          controller: 'SettingsCtrl'
        }
      }
    });  
  }

})();

/* global angular */

(function() {
  'use strict';

  angular
    .module('sports', [])
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    //console.log('config');
    $stateProvider

    .state('tab.sports', {
      url: '/sports',
      views: {
        'tab-sports': {
          templateUrl: 'html/modules/sports/sports/template.html',
          controller: 'SportsCtrl'
        }
      }
    })
    .state('tab.sport-detail', {
      url: '/sports/:id',
      views: {
        'tab-sports': {
          templateUrl: 'html/modules/sports/sport/template.html',
          controller: 'SportDetailCtrl'
        }
      }
    });  
  }

})();

/* global angular */

(function() {
  'use strict';

  angular
    .module('tabs', [])
    .config(config);

  /* @ngInject */
  function config($stateProvider) {
    //console.log('config');
    $stateProvider

    .state('tab', {
      url: '/tab',
      abstract: true,
      templateUrl: 'html/modules/tabs/template.html'
    });
  }
  
})();

/* global window, cordova, StatusBar, angular */

(function() {
  'use strict';
  
  angular
    .module('starter', [
      'ionic',
      'config',
      'index',
      'service',
      'settings',
      'sports',
      'tabs',
      'history',
      'interceptors'
  ])
    .config(config)
    .run(run);

  /* @ngInject */
  function run($ionicPlatform) {
    $ionicPlatform.ready(function() {
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  }

  /* @ngInject */
  function config($urlRouterProvider, $httpProvider) {
    $httpProvider.interceptors.push('TokenInterceptor');
    
    $urlRouterProvider.otherwise('/tab/dash');
  }

})();

/* global angular */

(function() {
  'use strict'
  angular
    .module('config', [])
    .constant('APP_CONFIG', {
      REST_ROOT: 'http://localhost:3000/',
      SPORT_MODAL_TEMPLATE: 'html/modules/settings/modals/add-sport/template.html',
      LOGIN_MODAL_TEMPLATE: 'html/modules/settings/modals/login/template.html'
    });
  
})();

 /* global angular */

(function() {
  'use strict';

  angular
    .module('interceptors')
    .factory('TokenInterceptor', TokenInterceptor);
    
    function TokenInterceptor($window, $log, LStorage) {
      var service = {
        'request': onRequest,
        'response': onResponse
      };
      
      return service;
      
      function onRequest(config) {
        if (config.url.includes('.html')) {
          return config;
        }
        var token = LStorage.getObject('myKey').token;
        if (!token) {
          return config;
        }
        config.headers['Authorisation'] = token;
        
        return config;
      }

      function onResponse(response) {
        if ($window.ANGULAR_DEBUG) {
          $log.log(response);
        }
        return response;
      }

    }

})();

/* global localStorage, angular */

(function() {
  'use strict'
  
  angular
    .module('service')
    .factory('LStorage', LStorage);
  
  function LStorage() {
    var LSservice = {
      setObject: setObject,
      getObject: getObject,
      setItem: setItem,
      getItem: getItem,
      clear: clear
    };
    
    return LSservice;
    
    function setObject(key, object) {
      var serializedObj = JSON.stringify(object);
      localStorage.setItem(key, serializedObj);
    }
    
    function getObject(key) {
      var stringObject = localStorage.getItem(key);
      if (stringObject) {
        try {
          var obj = JSON.parse(stringObject);
        }
        catch(err) {
          
        }
      }
      return obj;
    }
    
    function setItem(key, value) {
      localStorage.setItem(key, value);
    }
    
    function getItem(key) {
      return localStorage.getItem(key);
    }
    
    function clear() {
      localStorage.clear();
    }
  }

})();

/* global angular */
(function() {
  'use strict';

  angular
    .module('service')
    .factory('Pagination', Pagination);

  function Pagination($rootScope, $log) {

    var DEFAULTS = {
      limit: 10,
      offset: 0
    };

    function Paging(options) {
      this.options = options;

      this.service = options.service;
      this.key = options.key;
      this.onDone = options.onDone;
      this.params = angular.extend({}, DEFAULTS, options.params || {});
      this._items = [];
      this._status = {
        page: 0,
        progress: false,
        done: false,
        paginated: false
      };
    }

    angular.extend(Paging.prototype, {
      next: next,
      items: items,
      status: status,
      reload: reload
    });

    function next () {
      if (this._status.done || this._status.progress) {
        return;
      }
      this._status.progress = true;
      this._status.paginated = true;

      return this
        .service(this.params)
        .then(onSuccess().bind(this), onError.bind(this))
        .finally(onFinally.bind(this));
    }

    function onSuccess () {
      return function (response) {
        this._status.page++;
        Array.prototype.push.apply(this._items, response.data[this.key]);
        this.params.offset += response.data[this.key].length;
        if (response.data[this.key].length < this.params.limit) {
            this._status.done = true;
          }
        if (typeof this.onDone === 'function') {
          this.onDone();
        }
      }
    }

    function reload() {
      this._status.done = false;
      this._status.page = 0;
      this._status.paginated = false;
      this._items.length = 0;
      this.params.offset = 0;
      this.next();
    }

    function onError (error) {
      $log.log(error);
    }

    function onFinally () {
      this._status.progress = false;
    }

    function items () {
      return this._items;
    }

    function status () {
      return this._status;
    }

    return Paging;
  }

})();

/* global angular */

(function() {
  'use strict'
  
  angular
    .module('service')
    .factory('SportAPI', SportAPI);
  
  function SportAPI($http, APP_CONFIG){
    var service = {
      all: all,
      olympic: olympic,
      find: find,
      review: review,
      update: update,
      create: create,
      remove: remove,
      show: show,
      auth: auth
    };
    
    return service;
    
    function all(params) {
      var DEFAULTS = { limit: 10, offset: 0 };
      params = angular.extend(DEFAULTS, params);
      return $http.get(APP_CONFIG.REST_ROOT + 'sports?limit=' + params.limit + '&offset=' + params.offset);     
    }
    
    function olympic(params) {
      var DEFAULTS = { limit: 10, offset: 0 };
      params = angular.extend(DEFAULTS, params);
      return $http.get(APP_CONFIG.REST_ROOT + 'history?limit=' + params.limit + '&offset=' + params.offset);     
    }

    function find(id) {
      return $http.get(APP_CONFIG.REST_ROOT + 'sports/' + id); 
    } 
    
    function review(id) {
      return $http.get(APP_CONFIG.REST_ROOT + 'history/' + id); 
    } 
    
    function update() {
      return $http.get(APP_CONFIG.REST_ROOT + 'sports'); 
    }
    
    function create(sport) {
      return $http.post(APP_CONFIG.REST_ROOT + 'sports', { sport: sport }); 
    }
    
    function remove(id) {
      return $http.delete(APP_CONFIG.REST_ROOT + 'sports/' + id);
    }
    
    function show() {
      return $http.get(APP_CONFIG.REST_ROOT + 'pictures');
    }
    
    function auth(formAuth) {
      return $http.post(APP_CONFIG.REST_ROOT + 'auth', {formAuth: formAuth});
    }
  }

})();

/* global angular */

(function() {
  'use strict';

  angular
    .module('history')
    .controller('HistoryCtrl', HistoryCtrl);

  /* @ngInject */
  function HistoryCtrl($scope, SportAPI, Pagination) {
    
    var olympPage = new Pagination({
      service: SportAPI.olympic,
      onDone: function() {
        if (olympPage.status().page === 1) {
          $scope.$broadcast('scroll.refreshComplete');
        } else {
          $scope.$broadcast('scroll.infiniteScrollComplete');
        }
      },
      key: 'olympics'
    });
    
    angular.extend($scope, {
      paging: olympPage,
      olympics: olympPage.items(),
      status: olympPage.status()
    });
    
    olympPage.next();
  }  
})();

/* global angular */

(function() {
  'use strict';

  angular
    .module('history')
    .controller('ReviewCtrl', ReviewCtrl);

  /* @ngInject */
  function ReviewCtrl($scope, $stateParams, SportAPI) {
    angular.extend($scope, {
      
    });
  
    SportAPI
      .review($stateParams.id)
      .then(function(response) {
        $scope.review = response.data.review;
      });
  }

})();

/* global angular */

(function() {
  'use strict';

  angular
    .module('index')
    .controller('DashCtrl', DashCtrl);

  /* @ngInject */
  function DashCtrl($scope, $stateParams, SportAPI) {
    angular.extend($scope, {
      
    });
  
    SportAPI
      .show()
      .then(function(response) {
        $scope.pictures = response.data.pictures;
      });
    
  }

})();

/* global angular */

(function() {
  'use strict';

  angular
    .module('settings')
    .controller('SettingsCtrl', SettingsCtrl);

  /* @ngInject */
  function SettingsCtrl($scope, AddSportModal, LoginModal) {
    
   angular.extend($scope, {
      sportShow: sportShow,
      loginShow: loginShow
    });
    
    function sportShow() {
      AddSportModal.open();
    }
    
    function loginShow() {
      LoginModal.open();
    }
  }

})();

/* global angular */

(function() {
  'use strict';

  angular
    .module('sports')
    .controller('SportDetailCtrl', SportDetailCtrl);

  /* @ngInject */
  function SportDetailCtrl($scope, $stateParams, SportAPI) {
    angular.extend($scope, {
      
    });
  
    SportAPI
      .find($stateParams.id)
      .then(function(response) {
        $scope.sport = response.data.sport;
      });
  }

})();

/* global angular */

(function() {
  'use strict';

  angular
    .module('sports')
    .controller('SportsCtrl', SportsCtrl);

  /* @ngInject */
  function SportsCtrl($scope, $stateParams, SportAPI, Pagination, $state) {
    
    var paging = new Pagination({
      service: SportAPI.all,
      onDone: function() {
        if (paging.status().page === 1) {
          $scope.$broadcast('scroll.refreshComplete');
        } else {
          $scope.$broadcast('scroll.infiniteScrollComplete');
        }
      },
      key: 'sports'
    });
    
    angular.extend($scope, {
      paging: paging,
      sports: paging.items(),
      status: paging.status(),
      remove: remove
    });
    
    paging.next();
    
    function remove(index) {
      SportAPI
        .remove(paging.items()[index]['_id'])
        .then(function(/*response*/) {
          paging.items().splice(index, 1);
          $state.go('tab.sports');
        });
    }
  }  
})();

/* global angular */

(function() {
  'use strict';

  angular
    .module('settings')
    .controller('AddSportModalCtrl', AddSportModalCtrl); 
  
  function AddSportModalCtrl($scope, $state, SportAPI, AddSportModal) {

    angular.extend($scope, {
      sport: {}
    });

    angular.extend($scope, {
      create: create,
      close: close
    });
    
    function create(form) {
      if (form.$invalid) {
        return;
      }
      SportAPI
        .create($scope.sport)
        .then(function (/*response*/) {
          $state.go('tab.sports');  
        });
    }
    
    function close() {
      AddSportModal.close();
    }
  }
})();

/* global angular */

(function() {
  'use strict'

  angular
    .module('service')
    .factory('AddSportModal', AddSportModal);

  function AddSportModal($ionicModal, APP_CONFIG, $rootScope){
    var scope = $rootScope.$new();
    
    var service = {
      open: open,
      close: close
    };
    
    $ionicModal.fromTemplateUrl(APP_CONFIG.SPORT_MODAL_TEMPLATE, {
      animation: 'slide-in-up',
      scope: scope
    }).then(function (modal) {
      scope.AddSportModal = modal;
    });
    
    return service;

    function open() {
      return scope.AddSportModal.show();
    }

    function close() {
      return scope.AddSportModal.hide();
    } 
  }

})();

/* global angular */

(function() {
  'use strict';

  angular
    .module('settings')
    .controller('LoginModalCtrl', LoginModalCtrl);
  
  function LoginModalCtrl($scope, $ionicModal, $state, SportAPI, LStorage, LoginModal) {
    angular.extend($scope, {
      formAuth: {}
    });

    angular.extend($scope, {
      auth: auth,
      close: close
    });

    function auth(form){
      if (form.$invalid) {
        return;
      }
      SportAPI
        .auth($scope.formAuth)
        .then(function (response) {
          LStorage.setObject('myKey', response.data.tokenObject);
          $state.go('tab.sports');
      });
    }
    
    function close() {
      LoginModal.close();
    }
  }
})();

/* global angular */

(function() {
  'use strict'

  angular
    .module('service')
    .factory('LoginModal', LoginModal);

  function LoginModal($ionicModal, APP_CONFIG, $rootScope){
    var scope = $rootScope.$new();
    scope.data = {};
    
    var service = {
      open: open,
      close: close
    };

    $ionicModal.fromTemplateUrl(APP_CONFIG.LOGIN_MODAL_TEMPLATE, {
      animation: 'slide-in-up',
      scope: scope
    }).then(function (modal) {
      scope.LoginModal = modal;
    }); 
    
    
    return service;

    function open() {
      scope.LoginModal.show();
    }

    function close() {  
      scope.LoginModal.hide();
    } 
  }

})();
